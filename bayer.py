import numpy
import h5py
from PyQt5 import Qt
import pyqtgraph as pg


app = Qt.QApplication([])
view = pg.GraphicsView()
l = pg.GraphicsLayout(border=(100,100,100))
view.setCentralItem(l)
view.setWindowTitle('bayer view')
view.resize(800,600)


f = h5py.File('kk3.h5')

imgs = numpy.array(f['imgs'])

rx, ry = 1, 1
gx, gy = 0, 1
Gx, Gy = 1, 0
bx, by = 0, 0

a = imgs[0]

r = a[rx::2,ry::2]
g = (a[gx::2,gy::2] + a[gx::2,gy::2]) // 2
b = a[bx::2,by::2]

rgb = numpy.stack((r,g,b), axis=-1)

for i, q in enumerate((rgb, r, g, b)):
    if i == 2:
        l.nextRow()
    
    vb = l.addViewBox(lockAspect=True)
    img = pg.ImageItem(q)
    vb.addItem(img)
    
    print(i, q.shape, q.dtype)

view.show()
app.exec_()


