
import os
import time
import picamera
import picamera.array
import numpy as np
from gpiozero import LED
from PyQt5 import QtWidgets, QtGui, QtCore
import pyqtgraph as pg


class VidProcesor(picamera.array.PiRGBAnalysis):
    def __init__(self, camera):
        super(VidProcesor, self).__init__(camera)
        
    def analyze(self, a):
        print(a[60,90])

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, led_pin=2):
        super(MainWindow, self).__init__()
        
        self.resize(800,800)
        self.setWindowTitle('Color Analysis')
        
        self._cam = picamera.PiCamera()
        self._led = LED(led_pin)
        self._imv = pg.ImageView()
        self._vidproc = None
        
        self._init_camera()
        self._init_preview()
        
        # image widget
        self.setCentralWidget(self._imv)
        
        # create toolbar
        self.toolBar = self.addToolBar('Main')
        self.toolBar.setObjectName("mainTB")
        
        # define actions
        
        self.recVideoAction = self.toolBar.addAction('RecVideo')
        self.recVideoAction.setCheckable(True)
        self.recVideoAction.toggled.connect(self.onRecVideoToggled)
        
        self.multiPicAction = self.toolBar.addAction('RecVideo')
        self.multiPicAction.setCheckable(True)
        self.multiPicAction.toggled.connect(self.onMultiPicActionToggled)
        
        self.photoAction = self.toolBar.addAction('Photo')
        self.photoAction.triggered.connect(self.acquire_image)
        
        self.ledAction = self.toolBar.addAction('LED')
        self.ledAction.setCheckable(True)
        self.ledAction.toggled.connect(self.switch_led)
        
        self._previewTimer.start()
        
    def _init_preview(self):
        # self._vidproc = VidProcesor(self._cam)
        self._vidproc = picamera.array.PiRGBArray(self._cam, )
        self._cam.start_recording(self._vidproc, 'rgb')
        
        self._previewTimer = QtCore.QTimer()
        self._previewTimer.setInterval(500)
        self._previewTimer.timeout.connect(self._update_preview)
        
    def _update_preview(self):
        # data = self._vidproc.array
        # if data is None:
        #     print('no data')
        #     return
        #self._cam.capture(self._vidproc, 'rgb')
        #data = self._vidproc.array
        #print(data.shape)
        #self._imv.setImage(data, xvals=np.linspace(1., 3., data.shape[0]))
        
        with picamera.array.PiRGBArray(self._cam) as output:
            self._cam.capture(output, 'rgb')
            rgb = output.array
            print(rgb.shape)
            self._imv.setImage(rgb)
        return
        
        with picamera.array.PiBayerArray(self._cam) as output:
            self._cam.capture(output, 'jpeg', bayer=True)
            # bayer = output.demosaic()[:300,400:,:]
            bayer = output.array
            print(bayer.shape)
            self._imv.setImage(bayer)
    
        
    def _init_camera(self):
        self._cam.resolution = (320, 240)
        self._cam.framerate = 10
   
    def closeEvent(self, event):
        # self._cam.stop_recording()
        self._previewTimer.stop()
        self._cam.close()
        self._led.off()
        event.accept()
        
    def switch_led(self, on):
        """switch LED on and off"""
        if on:
            self._led.on()
        else:
            self._led.off()
            
    def onMultiPicActionToggled(self, on):
        if on:
            fname = os.path.join(os.path.dirname(__file__), 'output', 'test{timestamp:%Y%M%S%f}.jpg')
            # capture images until the button is unchecked 
            while self.multiPicAction.checked():
                print('dummypic')
                
            
    def onRecVideoToggled(self, on):
        if on:
            fname = os.path.join(os.path.dirname(__file__), 'output', 'test')
            self._cam.start_recording(fname, format='h264')
        else:
            self._cam.stop_recording()
            
    def acquire_image(self):
        data = np.empty((240, 320, 3), dtype=np.uint8)
        self._cam.capture(data, 'rgb')
        self._imv.setImage(data, xvals=np.linspace(1., 3., data.shape[0]))



if __name__ == '__main__':
    import sys
    app = QtGui.QApplication([])
    
    w = MainWindow()
    w.show()
    
    
    sys.exit(app.exec_())
